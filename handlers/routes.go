package handlers

import (
	"fmt"

	"accounts.dev/users/config"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var e = echo.New()
var cfg = config.New("dev")

// Start starts the server

func Start() {
	port := cfg.MustString("server.port")
	host := cfg.MustString("server.host")
	e.Pre(middleware.RemoveTrailingSlash())
	e.GET("/", Welcome)
	e.GET("/accounts", GetAccounts)
	e.GET("accounts/:id", GetAccount)
	e.POST("/accounts", CreateAccount)

	e.Logger.Printf("Listening on port %s", port)
	e.Logger.Fatal(e.Start(fmt.Sprintf("%s:%s", host, port)))
}
