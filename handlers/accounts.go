package handlers

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"accounts.dev/users/config"
	"accounts.dev/users/db"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	_ "github.com/lib/pq"
)

var (
	q *db.Queries
	v = validator.New()
)

// init initializes the database connection and the db Query struct
func init() {
	DB, err := sql.Open("postgres", dsn())
	if err != nil {
		log.Fatalf("Error connecting to database %+v", err)
	}

	q = db.New(DB)
}
func dsn() string {
	cfg = config.New("dev")
	return fmt.Sprintf("user=%s dbname=%s sslmode=%s password=%s",
		cfg.MustString("db.user"),
		cfg.MustString("db.name"),
		cfg.MustString("db.sslmode"),
		cfg.MustString("db.password"))
}

// Account validator is customizes playground validator for accounts struct
type AccountValidator struct {
	validator *validator.Validate
}

// Validate implements the echo.Validate function to validate accounts req body
func (a *AccountValidator) Validate(i interface{}) error {
	return a.validator.Struct(i)
}

// CreateAccount creates a new account into the database and returns the username
// of the created user
func CreateAccount(c echo.Context) error {
	var reqBody db.CreateAccountParams
	var err error
	c.Echo().Validator = &AccountValidator{validator: v}
	if err := c.Bind(&reqBody); err != nil {
		return c.JSON(http.StatusBadRequest, "error binding request "+err.Error())
	}
	if err := c.Validate(reqBody); err != nil {
		c.JSON(http.StatusBadRequest, "error "+err.Error())
	}
	user, err := q.CreateAccount(context.Background(), reqBody)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "error creating account "+err.Error())
	}
	return c.JSON(http.StatusCreated, user)
}

// GetAccount retrieves a single account with the given ID from the database
func GetAccount(c echo.Context) error {
	ID, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, "parsing uuid error"+err.Error())
	}
	acc, err := q.GetUser(context.Background(), ID)
	if err != nil {
		c.JSON(http.StatusBadRequest, "error getting user"+err.Error())
	}
	return c.JSON(http.StatusOK, acc)
}

// Get accounts returns all the accounts currently in the database
func GetAccounts(c echo.Context) error {
	users, err := q.GetUsers(context.Background())
	if err != nil {
		return c.JSON(http.StatusBadRequest, "error retrieving users "+err.Error())
	}
	return c.JSON(http.StatusOK, users)
}

// Welcome simply prints a welcome message to the user on the landing page
func Welcome(c echo.Context) error {
	return c.JSON(http.StatusOK, "Welcome to our Accounts page")
}
