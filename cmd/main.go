package main

import "accounts.dev/users/handlers"

// main function calls Start method from handlers package to run the server.
func main() {
	handlers.Start()
}
