package config

import (
	"log"
	"strings"

	"github.com/spf13/viper"
)

type Config struct {
	V        *viper.Viper
	Filename string
}

// New returns a pointer to a configuration with the given settings
func New(filename string) *Config {
	v := viper.New()

	if filename != "" {
		v.AddConfigPath("./config/")
		v.AddConfigPath(".")
		v.AutomaticEnv()
		v.SetConfigType("yaml")
		v.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
		v.SetConfigName(filename)
		v.SetEnvPrefix("/accounts")
		if err := v.ReadInConfig(); err != nil {
			log.Fatalf("cannot read config file, error: %s", err.Error())
		}

	} else {
		log.Fatal("Configuration file required")
	}
	return &Config{V: v, Filename: filename}
}

// Required ensures that the value of the given key is set/provided
// param is the key whose value is returned
func (c *Config) Required(param string) {
	if !c.V.IsSet(param) {
		log.Fatalf("Missing required parameter %s", param)
	}
}

// MustString returns a string value of the given key
// param is a key whose value is returned
func (c *Config) MustString(param string) string {
	c.Required(param)
	return c.V.GetString(param)
}

// MustString returns an int value of the given key
// param is a key whose value is returned
func (c *Config) MustInt(param string) int {
	c.Required(param)
	return c.V.GetInt(param)
}
