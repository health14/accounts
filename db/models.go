package db

import (
	"github.com/google/uuid"
)

type Account struct {
	ID        uuid.UUID `json:"id"`
	Username  string    `json:"username" validate:"required,lt=10"`
	Firstname string    `json:"firstname" validate:"required"`
	Lastname  string    `json:"lastname" validate:"required"`
	Phone     string    `json:"phone" validate:"required,e164" `
	Email     string    `json:"email" validate:"email"`
}
