package db

import (
	"context"

	"github.com/google/uuid"
)

const createAccount = `-- name: CreateAccount :one
INSERT INTO accounts(
    username, firstname, lastname, email, phone
) VALUES (
    $1, $2, $3, $4, $5
)
RETURNING id, username, firstname, lastname, phone, email
`

// CreateAccountParams are the parameters expected from the client for account creation
type CreateAccountParams struct {
	Username  string `json:"username" validate:"required,lt=10"`
	Firstname string `json:"firstname" validate:"required"`
	Lastname  string `json:"lastname" validate:"required"`
	Email     string `json:"email" validate:"email"`
	Phone     string `json:"phone" validate:"required,e164"`
}

// CreateAccount creates a new account and returns the content of the account created or error
func (q *Queries) CreateAccount(ctx context.Context, arg CreateAccountParams) (Account, error) {
	row := q.queryRow(ctx, q.createAccountStmt, createAccount,
		arg.Username,
		arg.Firstname,
		arg.Lastname,
		arg.Email,
		arg.Phone,
	)
	var i Account
	err := row.Scan(
		&i.ID,
		&i.Username,
		&i.Firstname,
		&i.Lastname,
		&i.Phone,
		&i.Email,
	)
	return i, err
}

const getUser = `-- name: GetUser :one
SELECT id, username, firstname, lastname, phone, email FROM accounts
WHERE ID = $1 LIMIT 1
`

func (q *Queries) GetUser(ctx context.Context, id uuid.UUID) (Account, error) {
	row := q.queryRow(ctx, q.getUserStmt, getUser, id)
	var i Account
	err := row.Scan(
		&i.ID,
		&i.Username,
		&i.Firstname,
		&i.Lastname,
		&i.Phone,
		&i.Email,
	)
	return i, err
}

const getUsers = `-- name: GetUsers :many
SELECT id, username, firstname, lastname, phone, email FROM accounts
ORDER BY lastname
`

func (q *Queries) GetUsers(ctx context.Context) ([]Account, error) {
	rows, err := q.query(ctx, q.getUsersStmt, getUsers)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []Account
	for rows.Next() {
		var i Account
		if err := rows.Scan(
			&i.ID,
			&i.Username,
			&i.Firstname,
			&i.Lastname,
			&i.Phone,
			&i.Email,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
